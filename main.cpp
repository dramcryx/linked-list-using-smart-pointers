#include "linked-list.cpp"
#include <iostream>

template< typename T >
inline void printList(LinkedList<T> &l)
{
    for (auto &i: l)
    {
        std::cout << i << " ";
    }
    std::cout << '\n';
}

int main()
{
    LinkedList<int> list;
    // 0 -> end
    // 1 -> 0 -> end and so on

    //check on empty list
    try
    {
        *list.begin();
    }
    catch (std::exception &e)
    {
        std::cout << e.what() << '\n';
    }
    try
    {
        *list.end();
    }
    catch (std::exception &e)
    {
        std::cout << e.what() << '\n';
    }

    //fill with numbers
    for (int i = 0; i < 10; ++i)
    {
        list.push_front(i);
    }

    //check if back and front work
    std::cout << "LIST FRONT " << list.front() << " LIST BACK " << list.back() << '\n';
    printList(list);

    //erase after beginning
    list.erase_after(list.begin());
    std::cout << "ERASED AFTER LIST FRONT, NOW: " << list.front() << ", LIST FRONT NEXT " << *(++list.begin()) << '\n';
    printList(list);

    //erase after end
    if (list.erase_after(list.end()) == list.end())
    {
        std::cout << "TRYING TO ERASE AFTER END, NO CHANGES\n";   
    }
    printList(list);

    //erase front
    list.pop_front();
    std::cout << "ERASE LIST FRONT, NOW: " << list.front() << '\n';
    printList(list);

    //insert after beginning
    list.insert_after(list.begin(), 11);
    std::cout << "INSERTED AFTER LIST FRONT, NOW: " << *(++list.begin()) << '\n';
    printList(list);

    //insert after end
    if (list.insert_after(list.end(), 1232) == list.end())
    {
        std::cout << "TRYING TO ISERT AFTER END, NO CHANGES\n";
    }
    printList(list);

    //insert after last element
    auto it = list.begin();
    for (int i = 0; i < list.size() - 1; ++i)
    {
        ++it;
    }
    auto res = list.insert_after(it, 6);
    std::cout << "INSERTED AFTER LAST ELEMENT, NOW: " << list.back() << '\n';
    printList(list);

    //check if back is correct if it was removed
    it = list.begin();
    for (int i = 0; i < list.size() - 2; ++i)
    {
        ++it;
    }
    list.erase_after(it);
    std::cout << "ERASED LAST ELEMENT, NOW: " << list.back() << '\n';
    printList(list);

    //check clearance
    list.clear();
    std::cout << "LIST CLEARED, SIZE: " << list.size() << '\n';
    return 0;
}
