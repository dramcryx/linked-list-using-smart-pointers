#ifndef LINKED_LIST_CLASS
#define LINKED_LIST_CLASS

#include <memory>
#include <iterator>

template< typename T >
struct node
{
    T element;
    std::unique_ptr<node<T>> next;
};

template< typename T >
class LinkedList;

template< typename T >
class ForwardIterator: public std::iterator< std::forward_iterator_tag, T, void, T*, T&>
{
    public:
    friend class LinkedList<T>;
    ForwardIterator(node<T> * element);
    void operator delete(void * ptr) = delete;
    void * operator new(std::size_t count) = delete;
    ForwardIterator<T> & operator++();
    ForwardIterator<T> operator++(int);
    bool operator==(ForwardIterator rhs);
    bool operator!=(ForwardIterator rhs);
    T& operator*() const;

    private:
    node<T> * m_element;
};

template< typename T >
class LinkedList
{
    public:
    using iterator = ForwardIterator<T>;
    LinkedList();

    void push_front(const T & val);
    void pop_front();
    iterator erase_after(iterator it);
    iterator insert_after(iterator it, const T & val);
    void clear();

    iterator begin();
    iterator end();

    T front() const;
    T back() const;
    std::size_t size() const;

    private:
    std::unique_ptr<node<T>> m_front;

    node<T> * m_back;
    std::size_t m_size;
};

#endif
