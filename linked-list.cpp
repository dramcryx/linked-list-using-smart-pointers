#include "linked-list.hpp"

template< typename T >
ForwardIterator<T>::ForwardIterator(node<T> * element): m_element(element)
{}

template< typename T >
ForwardIterator<T> & ForwardIterator<T>::operator++()
{
    m_element = m_element->next.get();
    return *this;
}

template< typename T >
ForwardIterator<T> ForwardIterator<T>::operator++(int)
{
    ForwardIterator<T> copy = *this;
    ++(*this);
    return copy;
}

template< typename T >
bool ForwardIterator<T>::operator==(ForwardIterator rhs)
{
    return m_element == rhs.m_element;
}

template< typename T >
bool ForwardIterator<T>::operator!=(ForwardIterator rhs)
{
    return m_element != rhs.m_element;
}

template< typename T >
T& ForwardIterator<T>::operator*() const
{
    if (!m_element)
    {
        throw std::runtime_error("Null reference");
    }
    return m_element->element;
}

template <typename T>
LinkedList<T>::LinkedList() : m_size(0),
                              m_back(nullptr)
{}

template< typename T >
void LinkedList<T>::push_front(const T & val)
{
    auto tmp = std::make_unique<node<T>>();
    tmp->element = val;
    if (m_front)
    {
        tmp->next = std::move(m_front);
    }
    else
    {
        m_back = tmp.get();
    }
    m_front = std::move(tmp);
    ++m_size;
}

template< typename T >
void LinkedList<T>::pop_front()
{
    if (m_front)
    {
        m_front = std::move(m_front->next);
        --m_size;
    }
}

template< typename T >
typename LinkedList<T>::iterator LinkedList<T>::erase_after(typename LinkedList<T>::iterator it)
{
    if (it == end() || !it.m_element->next.get())
    {
        return it;
    }
    if (it.m_element->next.get() == m_back)
    {
        it.m_element->next = std::move(it.m_element->next->next);
        m_back = it.m_element;
    }
    else
    {
        it.m_element->next = std::move(it.m_element->next->next);
    }
    --m_size;
    return it;
}

template< typename T >
typename LinkedList<T>::iterator LinkedList<T>::insert_after(typename LinkedList<T>::iterator it, const T & val)
{
    if (m_size == 0 || it == end())
    {
        return it;
    }
    auto insertable = std::make_unique<node<T>>();
    insertable->element = val;
    insertable->next = std::move(it.m_element->next);
    if (it.m_element == m_back)
    {
        m_back = insertable.get();
    }
    it.m_element->next = std::move(insertable);
    it.m_element = it.m_element->next.get();
    ++m_size;
    return it;
}

template< typename T >
void LinkedList<T>::clear()
{
    for (int i = 0; i < m_size; ++i)
    {
        m_front = std::move(m_front->next);
    }
    m_size = 0;
}

template< typename T >
typename LinkedList<T>::iterator LinkedList<T>::begin()
{
    return iterator(m_front.get());
}

template< typename T >
typename LinkedList<T>::iterator LinkedList<T>::end()
{
    return iterator(m_back ? m_back->next.get() : nullptr);
}

template< typename T >
T LinkedList<T>::front() const
{
    return m_front ? m_front->element : T{};
}

template< typename T >
T LinkedList<T>::back() const
{
    return m_back ? m_back->element : T{};
}

template< typename T >
std::size_t LinkedList<T>::size() const
{
    return m_size;
}
